import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import contract from 'truffle-contract';

import Web3 from 'web3';

declare let window: any;


@Injectable({
  providedIn: 'root'
})
export class Web3Service {
  private web3: any;
  private accounts: string[];
  public ready = false;
  public accountsObservable = new Subject<string[]>();


  // public accountsObservable = new Subject<string[]>();

  constructor() {
    /* window.addEventListener('load', (event) => {
      this.bootstrapWeb3();
    }); */
  }

  public bootstrapWeb3() {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      this.web3 = new Web3(window.web3.currentProvider);
    } else {
      console.log('No web3? You should consider trying MetaMask!');
      // Hack to provide backwards compatibility for Truffle, which uses web3js 0.20.x
      Web3.providers.HttpProvider.prototype.sendAsync = Web3.providers.HttpProvider.prototype.send;
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    }
  }


  public async enableProvider(): Promise<any> {

    // Check if Provider is in Privacy mode
    if ('enable' in this.web3.currentProvider) {
      await this.web3.currentProvider.enable();
    }

    return await this.web3.eth.getAccounts();
  }


  public async artifactsToContract(artifacts) {
    if (!this.web3) {
      const delay = new Promise(resolve => setTimeout(resolve, 100));
      await delay;
      return await this.artifactsToContract(artifacts);
    }

    const contractAbstraction = contract(artifacts);
    contractAbstraction.setProvider(this.web3.currentProvider);
    return contractAbstraction;

  }

  public getAccounts(): any{
    this.web3.eth.getAccounts((err, accs) => {
      this.accountsObservable.next(accs);
      this.accounts = accs;
    });
    return this.accounts;
  }
}
