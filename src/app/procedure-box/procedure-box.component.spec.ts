import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureBoxComponent } from './procedure-box.component';

describe('ProcedureBoxComponent', () => {
  let component: ProcedureBoxComponent;
  let fixture: ComponentFixture<ProcedureBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
