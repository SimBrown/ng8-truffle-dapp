import { Web3Service } from '../util/web3.service';
import { Component, Inject, OnInit, enableProdMode } from '@angular/core';

declare let require: any;

const vault_contract_artifact = require('../../../build/contracts/Vault.json');

@Component({
  selector: 'app-procedure-box',
  templateUrl: './procedure-box.component.html',
  styleUrls: ['./procedure-box.component.scss']
})
export class ProcedureBoxComponent implements OnInit {

  isLinear = true;

  address: string;
  fakeToken: string;
  fakeName: string;

  Vault: any;


  constructor(private web3Service: Web3Service) { }

  ngOnInit(): void {

    this.web3Service.bootstrapWeb3();

    this.web3Service.artifactsToContract(vault_contract_artifact)
      .then((VaultAbstraction) => {
        this.Vault = VaultAbstraction;
        this.Vault.deployed().then(deployed => {
          console.log(deployed);
          console.log('Abstraction');
          console.log(VaultAbstraction);
          deployed.DataSet({}, (err, ev) => {
            console.log('DataSet event came in.');
          });
        });
      });

    console.log(this.web3Service.getAccounts());
  }

  /* REMOVE */
  async started() {

    // Ask authorization and returns selected account
    this.web3Service.enableProvider().then((a) => { this.address = a; });

    this.fakeToken = 'a47f59b0fc06b7baa9f0364f76ddeab7047622b7ca615cbf5f91ef4321330648';
    this.fakeName = 'John Smith';

    this.sendTransaction();
  }

  async sendTransaction() {
    console.log('Sending a "SET" transaction');
  }




  storeValue() {

  }
}
