pragma solidity ^0.5.8;

contract Vault {
    string storedData;

    event DataSet(address indexed _from, string _value);

    function set(string memory x) public {
        storedData = x;
        emit DataSet(msg.sender, x);
    }

    function get() public view returns (string memory) {
        return storedData;
    }
}